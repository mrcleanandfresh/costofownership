<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 10:53 AM
 */

namespace Ownership\Cost;


interface CarCost {
	function costOfFuel( $gallons, $costOfFuel, $fuelCapacity );

	function costOfExpenses( array $expenses );

	function costOfTires( $treadLife, $costPerTire, $numTires );
}