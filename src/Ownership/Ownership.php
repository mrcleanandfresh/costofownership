<?php

namespace Ownership;

use Ownership\Cost\OwnershipCarCost;
use Ownership\Factory\OwnershipCarCostFactory;
use Symfony\Component\Config\Definition\Exception\Exception;

class Ownership {

	/**
	 * Find the Ownership Cost for a car
	 *
	 * @param $currOdo
	 * @param $prevOdo
	 *
	 * @param $fuelGallons
	 * @param $fuelCost
	 * @param $fuelCapacity
	 * @param $otherExpenses
	 * @param $tireTreadLife
	 * @param $tireCost
	 * @param $tireNumber
	 *
	 * @return OwnershipCarCost
	 */
	public static function findOwnershipCostForCar( $currOdo, $prevOdo, $fuelGallons, $fuelCost, $fuelCapacity, $otherExpenses, $tireTreadLife, $tireCost, $tireNumber ) {
		if ( $currOdo > $prevOdo ) {
			$occ = OwnershipCarCostFactory::getOwnershipCarCost( $currOdo, $prevOdo );
		} else {
			throw new Exception( "Please switch your Current Odometer, and Previous Odometer; your Previous Odometer can't be greater than your Current Odometer reading." );
		}

		return $occ->costPerMileToOperate( $fuelGallons, $fuelCost, $fuelCapacity, $otherExpenses, $tireTreadLife, $tireCost, $tireNumber );
	}
}