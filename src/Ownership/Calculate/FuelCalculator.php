<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 11:10 AM
 */

namespace Ownership\Calculate;


class FuelCalculator {
	use Calculator;

	/** @var int $_currentOdometer */
	private $_currentOdometer;
	/** @var  int $_previousOdometer */
	private $_previousOdometer;
	/** @var  float $_gallonsAdded */
	private $_gallonsAdded;
	/** @var  float $_fuelCost */
	private $_fuelCost;
	/** @var float $_fuelCapacity */
	private $_fuelCapacity;

	/**
	 * MilesPerGallonCalculator constructor.
	 *
	 * @param int   $currOdo
	 * @param int   $prevOdo
	 * @param float $gals
	 * @param float $gallonOfGasCost
	 * @param float $fuelCap
	 *
	 * @internal param float $cost
	 */
	public function __construct( $currOdo, $prevOdo, $gals, $gallonOfGasCost, $fuelCap ) {
		$this->_currentOdometer  = $this::sanitizeNumForCalc( $currOdo );
		$this->_previousOdometer = $this::sanitizeNumForCalc( $prevOdo );
		$this->_gallonsAdded     = $this::sanitizeNumForCalc( $gals );
		$this->_fuelCost         = $this::sanitizeNumForCalc( $gallonOfGasCost );
		$this->_fuelCapacity     = $this::sanitizeNumForCalc( $fuelCap );
	}

	/**
	 * Get miles per gallon calculation
	 * @return float
	 */
	public function getMilesPerGallon() {
		$miles = $this->_calcMilesTraveled( $this->_currentOdometer, $this->_previousOdometer );

		return $this->_calcMilesPerGallon( $miles );
	}

	private function _calcMilesTraveled( $currOdo, $prevOdo ) {
		return $currOdo - $prevOdo;
	}

	private function _calcMilesPerGallon( $milesTraveled ) {
		if ( $milesTraveled !== 0 ) { // Make sure we're not diving by zero
			return round( ( $milesTraveled / $this->_gallonsAdded ) * 100, 2 ) / 100;
		} else {
			throw new \Exception( "Please enter a mileage other than zero." );
		}
	}

	/**
	 * Get the cost per mile for fuel
	 * @return float
	 * @throws \Exception
	 */
	public function getCostPerMile() {
		$miles = $this->_calcMilesTraveled( $this->_currentOdometer, $this->_previousOdometer );
		$mpg   = $this->_calcMilesPerGallon( $miles );
		if ( $this->_fuelCost !== null && $this->_fuelCost !== 0 ) { // make sure we're not dividing by zero
			$cost = $this->_fuelCost / $mpg;
		} else {
			throw new \Exception( "Please enter a mileage other than zero." );
		}

		return round( $cost * 100, 2 ) / 100;
	}

	/**
	 * @return int
	 */
	public function getCurrentOdometer() {
		return $this->_currentOdometer;
	}

	/**
	 * @return int
	 */
	public function getPreviousOdometer() {
		return $this->_previousOdometer;
	}

	/**
	 * @return float
	 */
	public function getGallonsAdded() {
		return $this->_gallonsAdded;
	}

	/**
	 * @return float
	 */
	public function getFuelCost() {
		return $this->_fuelCost;
	}

	/**
	 * @return float
	 */
	public function getFuelCapacity() {
		return $this->_fuelCapacity;
	}
}