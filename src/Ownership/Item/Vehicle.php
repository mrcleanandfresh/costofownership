<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/6/2017
 * Time: 10:55 AM
 */

namespace Ownership\Item;


class Vehicle {
	private $_tireCount;
	private $_tireDiameter;

	private $_fuel;
	private $_fuelCapacity;

	public function __construct( $numTires, $tireDiam, $fuelGrade, $tankSize ) {
		$this->_tireCount    = $numTires;
		$this->_tireDiameter = $tireDiam;

		$this->_fuel         = $fuelGrade;
		$this->_fuelCapacity = $tankSize;
	}
}