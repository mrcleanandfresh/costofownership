<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 7/7/2017
 * Time: 3:12 PM
 */

namespace Ownership\UnitTest\Cost;


use Ownership\Calculate\CostsCalculator;
use Ownership\Calculate\FuelCalculator;
use Ownership\Calculate\TiresCalculator;
use Ownership\Cost\OwnershipCarCost;
use PHPUnit\Framework\TestCase;

class OwnershipCarCostTest extends TestCase {
	/** @var  OwnershipCarCost $ownershipCostCar */
	public $ownershipCostCar;

	public function setUp() {
		$this->ownershipCostCar = new OwnershipCarCost( 94087, 94370 );
	}

	public function testCostPerMileToOperate() {
		$gallons      = 14.564;
		$costOfFuel   = 2.239;
		$fuelCapacity = 19;
		$treadLife    = 60000;
		$tireCost     = 465;
		$numOfTires   = 4;
		$expenses     = [ 60, 76, 223.40, 34, 0.32 ];
		$tires        = new TiresCalculator( $treadLife, $tireCost, $numOfTires );
		$ve           = new CostsCalculator( $expenses, 94087 - 94370 );
		$fuel         = new FuelCalculator( 94087, 94370, $gallons, $costOfFuel, $fuelCapacity );

		$actual   = array(
			'costOfFuel'       => $fuel->getCostPerMile(),
			'costOfExpenses'   => $ve->getCostsPerMile(),
			'costOfTires'      => $tires->getCostPerMile(),
			'totalCostPerMile' => $fuel->getCostPerMile() + $ve->getCostsPerMile() + $tires->getCostPerMile()
		);
		$expected = $this->ownershipCostCar->costPerMileToOperate( $gallons, $costOfFuel, $fuelCapacity, $expenses, $treadLife, $tireCost, $numOfTires );

		$this->assertEquals( $expected, $actual, "Add the cost per mile for tires, costs and fuel to get the cost per mile to operate." );
	}
}